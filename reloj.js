
    var canvasDHours = document.getElementById("canvas-dhours");
    var canvasUHours = document.getElementById("canvas-uhours");
    var canvasDMinutes = document.getElementById("canvas-dminutes");
    var canvasUMinutes = document.getElementById("canvas-uminutes");
    var canvasDSeconds = document.getElementById("canvas-dseconds");
    var canvasUSeconds = document.getElementById("canvas-useconds");


    let ctxDHours = canvasDHours.getContext("2d");
    let ctxUHours = canvasUHours.getContext("2d");
    let ctxDMinutes = canvasDMinutes.getContext("2d");
    let ctxUMinutes = canvasUMinutes.getContext("2d");
    let ctxDSeconds = canvasDSeconds.getContext("2d");
    let ctxUSeconds = canvasUSeconds.getContext("2d");
    

    function reset(context) {
        context.fillStyle = "rgb(70,0,0)";

        context.fillRect(10, 10, 80, 10); //a
        context.fillRect(80, 10, 10, 70); //b
        context.fillRect(80, 70, 10, 70); //c
        context.fillRect(10, 130, 80, 10); //d
        context.fillRect(10, 80, 10, 60); //e
        context.fillRect(10, 10, 10, 70); //f
        context.fillRect(10, 70, 70, 10); //g
    }

    function actualizar() {
        reset(ctxDHours)
        reset(ctxUHours)
        reset(ctxDMinutes)
        reset(ctxUMinutes)
        reset(ctxDSeconds)
        reset(ctxUSeconds)

        var dia = new Date();
        var hora = dia.getHours();
        var minuto = dia.getMinutes();
        var segundo = dia.getSeconds();

        var i = 0;


        //COGER NUMEROS 
        var dHora = Math.floor(hora / 10);
        var uHora = hora % 10;
        var dMin = Math.floor(minuto / 10);
        var uMin = minuto % 10;
        var dSeg = Math.floor(segundo / 10);
        var uSeg = segundo % 10;
        // llamar a funcion pintar pasando id o contexto + unidad/decena
        
        pintar(dHora, ctxDHours)
        pintar(uHora, ctxUHours)
        pintar(dMin, ctxDMinutes)
        pintar(uMin, ctxUMinutes)
        pintar(dSeg, ctxDSeconds)
        pintar(uSeg, ctxUSeconds)

    }
    function pintar(numero, context) {
        let segments = [];

            switch (numero) {
            case 0:
                segments = ['a', 'b', 'c', 'd', 'e', 'f'];

                break;
            case 1:
                segments = ['b', 'c'];
    
                break;
            case 2:
                segments = ['a', 'b', 'd', 'e', 'g'];
        
                break;
            case 3:
                segments = ['a', 'b', 'c', 'd', 'g'];
          
                break;
            case 4:
                segments = ['b', 'c', 'f', 'g'];
             
                break;
            case 5:
                segments = ['a', 'c', 'd', 'f', 'g'];
          
                break;
            case 6:
                segments = ['a', 'c', 'd', 'e', 'f', 'g'];
         
                break;
            case 7:
                segments = ['a', 'b', 'c'];
       
                break;
            case 8:
                segments = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];
        
                break;
            case 9:
                segments = ['a', 'b', 'c', 'd', 'f', 'g'];
             
                break;
        
            
        }

        context.fillStyle = "rgb(200, 0, 0)";

        for (let j = 0; j < segments.length; j++) {
            var element = segments[j];

            switch (element) {
                case 'a':
                    context.fillRect(10, 10, 80, 10);
                    break;
                case 'b':
                    context.fillRect(80, 10, 10, 70);
                    break;
                case 'c':
                    context.fillRect(80, 70, 10, 70);
                    break;
                case 'd':
                    context.fillRect(10, 130, 80, 10);
                    break;
                case 'e':
                    context.fillRect(10, 80, 10, 60);
                    break;
                case 'f':
                    context.fillRect(10, 10, 10, 70);
                    break;
                case 'g':
                    context.fillRect(10, 70, 70, 10);
                    break;

            }

        }
    }

    actualizar()

    setInterval(actualizar, 1000);
